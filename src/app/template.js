export default ({ body }) => `
  <!DOCTYPE html>
    <html>
      <head>
        <title>React SSR</title>
        <script src='/bundle.js' defer></script>
        <link href='/styles.css' rel='stylesheet' type='text/css'/>
      </head>
      <body>
        <div id='app'>${body}</div>
      </body>
    </html>
`;
