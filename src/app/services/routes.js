import Home from '../scenes/Home';
import Test from '../scenes/Test';

const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/test',
    component: Test,
  },
];

export default routes;
