import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';

import template from '../../template';
import App from '../../App';

export const render = (req, res) => {
  const body = renderToString(
    <StaticRouter location={req.url} context={{}}>
      <App />
    </StaticRouter>
  );

  res.send(template({ body }));
};
