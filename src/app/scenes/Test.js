import React from 'react';
import { Link } from 'react-router-dom';

const Test = () =>
  <div>
    Test Page
    <div>
      <Link to='/'>Home</Link>
    </div>
  </div>;

export default Test;
