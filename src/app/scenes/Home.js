import React from 'react';
import { Link } from 'react-router-dom';

const Home = () =>
  <div>
    Home Page
    <div>
      <Link to='/test'>Test</Link>
    </div>
  </div>;

export default Home;
