import React from 'react';
import { Route, Switch } from 'react-router-dom';
import routes from './services/routes';
import NotFound from './scenes/NotFound';

import './App.scss';

const App = () =>
  <div>
    <Switch>
      {routes.map(({ path, exact, component: Component }) =>
        <Route key={path} path={path} exact={exact} render={(props) =>
          <Component {...props} />
        } />
      )}

      <Route render={(props) => <NotFound {...props} />} />
    </Switch>
  </div>;

export default App;
