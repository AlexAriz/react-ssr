import express from 'express';
import cors from 'cors';

import { render } from './app/services/utils/setup';

const DEFAULT_PORT = 8080;
const app = express();

app.use(cors());
app.use(express.static('public'));

app.get('*', render);

app.listen(DEFAULT_PORT, () => {
  console.log(`Server is listening on port: ${DEFAULT_PORT}`); // eslint-disable-line no-console
});
