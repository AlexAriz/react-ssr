const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const config = {
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        include: path.join(__dirname, 'src'),
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              camelCase: true,
              localIdentName: '[local]--[hash:base64:5]',
            },
          },
          'sass-loader',
        ],
      },
    ],
  },
};

const browserConfig = Object.assign({}, config, {
  entry: './src/browser.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new webpack.DefinePlugin({
      __isBrowser__: 'true',
    }),
    new MiniCssExtractPlugin({
      path: path.resolve(__dirname, 'public'),
      filename: 'styles.css',
      publicPath: '/',
    }),
  ],
});

const serverConfig = Object.assign({}, config, {
  entry: './src/server.js',
  target: 'node',
  externals: [nodeExternals()],
  output: {
    path: __dirname,
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new webpack.DefinePlugin({
      __isBrowser__: 'false',
    }),
    new MiniCssExtractPlugin({
      path: __dirname,
      filename: 'styles.css',
      publicPath: '/',
    }),
  ],
});

module.exports = [ browserConfig, serverConfig ];
